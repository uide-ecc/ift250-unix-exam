# IFT250-UNIX-Exam

This exam is about using the text editor to generate reports.
You might need to have two remote sessions at the same time. I suggest you use a Linux VM or the Windows CMD to have different sessions in tabs.

1. Under the directory you have been asked to create, copy all the content of the /etc directory. You can skip permission denials by using the redirection **2>/dev/null** as presented in the last lab. Answer the following questions within your text file as asked at the instructions in your Canvas.
    * How many files and directories do you have?
    * How many links regardless of being hard or symbolic do you have?
    * What happens when you open the file **resolv.conf**? Is it located in $HOME/exam.2/etc? 
    * How many files are executable?
    * How many directories do you have within this etc directory you copied?
    * What is the IP address of the DNS server for this Ubuntu? Hint: The information is located in the **hosts** file

2. Now you are going to generate a mini report from a DNS query log file. I need you to use the proper commands according to the requirements asked.
    * I need to see in a new file called **dns.localhost.query** all the queries produced by the loopback IP address. How many times was a DNS query sent by localhost? This should be in your answers file
        * From the file you just created, how many times there was a request for a Linux/UNIX/BSD domain?
        * How many times a pointer was addressed? (Hint: In DNS services, there are some acronyms, A for address, NS for mane server, MX for mail exchange, CNAME for Canonical Name and PTR for pointer)
        * How many times there was a query addressed to the domain ".bluenet"
    * How many warnings have been recorded? Get the information in a new file called **dns.warning.log**
        * How many times was unable to find the root NS?
        * Were any TTL issues popped? If yes, how many of them?
    * Finally, filter all the transfers in a file named **dns.transfer.query**
        * How many were completed?
        * How many failed? What were the reasons?

**Note:** Be aware that besides having the reports, you need to submit the answers in the file asked in the Canvas platform. Some answers need to be done via using the "command execution" feature that the text editor nano has.

Best of luck!

Jpp

    